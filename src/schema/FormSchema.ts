import * as Yup from "yup";

export default Yup.object().shape({
  name: Yup.string().required("Campo Obrigatório"),
  email: Yup.string().required("Campo Obrigatório").email("Email Inválido"),
  cpf: Yup.string().required("Campo Obrigatório"),
  dateOfBirth: Yup.string().required("Campo Obrigatório"),
  phone: Yup.string().required("Campo Obrigatório"),
  instagram: Yup.string().required("Campo Obrigatório"),
});
