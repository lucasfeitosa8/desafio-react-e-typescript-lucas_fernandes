import React from "react";

import SocialStyles from "./Social.module.css";

import PlusIcon from "../../icons/others/plus-icon.png";

import Facebook from "../../icons/social/facebook.png";
import Instagram from "../../icons/social/instagram.png";
import Linkedin from "../../icons/social/linkedin.png";
import Twitter from "../../icons/social/twitter.png";
import YouTube from "../../icons/social/youtube.png";

export const Social = () => {
  return (
    <>
      <div className={SocialStyles["social-container"]}>
        <nav className={SocialStyles["pages-links-wrapper"]}>
          <div className={SocialStyles["pages-links"]}>
            <strong>Institucional</strong>
            <p>Quem Somos</p>
            <p>Política de Privacidade</p>
            <p>Segurança</p>
            <p className={SocialStyles["seja-revendedor"]}>
              Seja um Revendedor
            </p>
            <img src={PlusIcon} alt="" className={SocialStyles["plus-icon"]} />
          </div>

          <div className={SocialStyles["pages-links"]}>
            <strong>Dúvidas</strong>
            <p>Entrega</p>
            <p>Pagamemto</p>
            <p>Troca e Devoluções</p>
            <p>Dúvidas Frequentes</p>
            <img src={PlusIcon} alt="" className={SocialStyles["plus-icon"]} />
          </div>

          <div className={SocialStyles["pages-links-contact"]}>
            <strong>Fale Conosco</strong>
            <p>Atendimento ao Consumidor</p>
            <p>(11) 4159 9504</p>
            <p>Atendimento Online</p>
            <p>(11) 99433-8825</p>
            <img src={PlusIcon} alt="" className={SocialStyles["plus-icon"]} />
          </div>
        </nav>

        <div className={SocialStyles["social-links"]}>
          <div className={SocialStyles["social-links-wrapper"]}>
            <ul>
              <li>
                <a
                  href="https://www.facebook.com/digitalm3/"
                  target={"_blank"}
                  rel="noreferrer"
                >
                  <img
                    src={Facebook}
                    alt="Icone com imagem do Facebook e link para página da M3"
                  />
                </a>
              </li>

              <li>
                <a
                  href="https://www.instagram.com/m3.ecommerce/?igshid=jp5yksg5apa2"
                  target={"_blank"}
                  rel="noreferrer"
                >
                  <img
                    src={Instagram}
                    alt="Icone com imagem do Instagram e link para página da M3"
                  />
                </a>
              </li>

              <li>
                <a
                  href="https://m3ecommerce.com/"
                  target={"_blank"}
                  rel="noreferrer"
                >
                  <img
                    src={Twitter}
                    alt="Icone com imagem do Twitter e link para página da M3"
                  />
                </a>
              </li>

              <li>
                <a
                  href="https://www.youtube.com/channel/UCW4o86gZG_ceA8CmHltXeXA/videos"
                  target={"_blank"}
                  rel="noreferrer"
                >
                  <img
                    src={YouTube}
                    alt="Icone com imagem do YouTube e link para página da M3"
                  />
                </a>
              </li>

              <li>
                <a
                  href="https://www.linkedin.com/company/m3ecommerce/"
                  target={"_blank"}
                  rel="noreferrer"
                >
                  <img
                    src={Linkedin}
                    alt="Icone com imagem do Linkedin e link para página da M3"
                  />
                </a>
              </li>
            </ul>

            <strong className={SocialStyles["website"]}>
              www.loremipsum.com
            </strong>
          </div>
        </div>
      </div>
    </>
  );
};
