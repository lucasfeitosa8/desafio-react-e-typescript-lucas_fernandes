import React, { useState } from "react";

import headerStyles from "./Header.module.css";
import searchBarStyles from "../searchBar/SearchBar.module.css";

import toogle from "../../icons/others/toggle-menu-icon.png";
import logo from "../../icons/others/m3-academy-logo.png";
import cart from "../../icons/others/shopping-cart.png";
import closeMenuIcon from "../../icons/others/close-image.png";

export const Header = () => {
  const [active, setActive] = useState(false);
  function OpenMenu(e: any) {
    setActive(!active);
  }
  //
  const SearchBar = () => {
    return (
      <input
        type="search"
        className={searchBarStyles["search-bar"]}
        placeholder="Buscar"
      />
    );
  };

  const MenuMobile = () => {
    return (
      <>
        <div className={headerStyles["menu-mobile"]}>
          <img
            src={closeMenuIcon}
            alt="Fechar Menu"
            className={headerStyles["close-menu-icon"]}
          />
          <div className={headerStyles["menu-opened-items"]}>
            <p>Entrar</p>
            <p>Cursos</p>
            <p>Saiba Mais</p>
          </div>
        </div>
      </>
    );
  };

  const MenuToogle = () => {
    return (
      <img src={toogle} alt="Menu" className={headerStyles["toogle-menu"]} />
    );
  };

  const Logo = () => {
    return (
      <img src={logo} alt="M3 Academy Logo" className={headerStyles["logo"]} />
    );
  };

  const Cart = () => {
    return (
      <div className={headerStyles["cart-and-text-wrapper"]}>
        <span>Entrar</span>

        <img
          src={cart}
          alt="Shopping Cart"
          className={headerStyles["shopping-cart"]}
        />
      </div>
    );
  };

  const [menuOpen, setMenuOpen] = useState(false);

  const toogleMenu = () => {
    setMenuOpen(!menuOpen);
  };

  return (
    <>
      <header className={menuOpen ? "header .menu-mobile" : "header"}>
        <div className={headerStyles["icons-wrapper"]}>
          {/* MenuMobile  refers to the menu open when the toogle is clicked*/}
          <MenuMobile />
          {/* MenuToogle it's the hamburger icon to be clicked and open the mobile menu */}
          <MenuToogle />
          <Logo />
          <Cart />
        </div>
        <SearchBar />
        <nav className={headerStyles["bottom-header-content"]}>
          <span className={headerStyles["bottom-header-text"]}>Cursos</span>
          <span className={headerStyles["bottom-header-text"]}>Saiba Mais</span>
        </nav>
      </header>
    </>
  );
};
