import React from "react";

import { Routes, Route, Link } from "react-router-dom";

import Sobre from "../pageContent/content/sobre/Sobre";
import Pagamento from "../pageContent/content/pagamento/Pagamento";
import Entrega from "../pageContent/content/entrega/Entrega";
import Troca from "../pageContent/content/troca/TrocaDevolucao";
import Seguranca from "../pageContent/content/seguranca/SegurancaPrivacidade";
import Contato from "../pageContent/content/contato/Contato";

import home from "../../icons/others/home-icon.png";
import arrowRight from "../../icons/others/arrow-to-right.png";

import MainContentStyles from "./MainContent.module.css";

export const Main = () => {
  return (
    <main>
      <section className={MainContentStyles["upper-icons-wrapper"]}>
        <img src={home} alt="Home" className={MainContentStyles["home-icon"]} />
        <img
          src={arrowRight}
          alt="Home"
          className={MainContentStyles["arrowRightIcon"]}
        />
        <p>Institucional</p>
      </section>

      <section className={MainContentStyles["institucional-content"]}>
        <strong className={MainContentStyles["institucional-title"]}>
          Institucional
        </strong>

        <article>
          <div className={MainContentStyles["list-wrapper"]}>
            <ul>
              <Link to="/sobre/Sobre" className={MainContentStyles["link"]}>
                <li>Sobre</li>
              </Link>
              <Link
                to="/pagamento/Pagamento"
                className={MainContentStyles["link"]}
              >
                <li>Formas de Pagamento</li>
              </Link>
              <Link to="/entrega/Entrega" className={MainContentStyles["link"]}>
                <li>Entrega</li>
              </Link>
              <Link
                to="/troca/TrocaDevolucao"
                className={MainContentStyles["link"]}
              >
                <li>Troca e Devolução</li>
              </Link>
              <Link
                to="/seguranca/SegurancaPrivacidade"
                className={MainContentStyles["link"]}
              >
                <li>Segurança e Privacidade</li>
              </Link>
              <Link to="/contato/Contato" className={MainContentStyles["link"]}>
                <li>Contato</li>
              </Link>
            </ul>
          </div>

          <div className={MainContentStyles["texts-wrapper"]}>
            <Routes>
              <Route path="/sobre/Sobre" element={<Sobre />} />
              <Route path="/pagamento/Pagamento" element={<Pagamento />} />
              <Route path="/entrega/Entrega" element={<Entrega />} />
              <Route path="/troca/TrocaDevolucao" element={<Troca />} />
              <Route
                path="/seguranca/SegurancaPrivacidade"
                element={<Seguranca />}
              />
              <Route path="/contato/Contato" element={<Contato />} />
            </Routes>
          </div>
        </article>
      </section>
    </main>
  );
};

export default Main;
