import React from "react";
import EntregaStyle from "./Entrega.module.css";

export default function Entrega() {
  return (
    <>
      <div className={EntregaStyle["texts-wrapper"]}>
        <strong>Entrega</strong>
        <p>
          A M3 está no mercado desde 2014, revolucionando negócios através do
          comércio digital, e transformando a experiência que as lojas virtuais
          proporcionam aos consumidores. Alinhados aos princípios de User
          Experience (UX), desenvolvemos e otimizamos e-commerces capazes de
          oferecer uma jornada de compra simples, intuitiva e rápida, derrubando
          os obstáculos e, consequentemente, conduzindo os visitantes à
          conversão.
        </p>

        <p>
          Somos certificados pela plataforma VTEX com o selo VTEX Implementation
          Expert Advanced, que revela a nossa alta expertise em implantar e
          evoluir e-commerces através da tecnologia. Além disso, estamos sempre
          antenados às tendências do comércio digital, a fim de levar a melhor
          solução aos clientes.
        </p>

        <p>
          Por entendermos que cada negócio é único, oferecemos um atendimento
          personalizado, baseado no estudo das necessidades específicas de cada
          loja virtual. Fazer o melhor uso dos recursos que o cliente já possui,
          também é o nosso objetivo. Por isso, vivemos em busca das soluções
          mais modernas e eficientes do mercado. Desta forma, cumprimos o nosso
          compromisso com os resultados.
        </p>
      </div>
    </>
  );
}
