import React, { useState } from "react";
import * as yup from "yup";
import { Formik, Form, Field, ErrorMessage, FormikHelpers } from "formik";
import ContatoStyle from "./Contato.module.css";
import FormSchema from "../../../../schema/FormSchema";

interface IFormikValues {
  name: string;
  email: string;
  cpf: string;
  dateOfBirth: string;
  phone: string;
  instagram: string;
}

const initialValues = {
  name: "",
  email: "",
  cpf: "",
  dateOfBirth: "",
  phone: "",
  instagram: "",
};

// (document.querySelector(".texts-wrapper") as HTMLFormElement).reset();

export default function Contato() {
  const handleFormikSubmit = async (values: IFormikValues, actions: any) => {
    console.log(values);
    await new Promise((resolve) => setTimeout(resolve, 10));
    actions.resetForm();
  };

  return (
    <>
      <Formik
        onSubmit={handleFormikSubmit}
        initialValues={initialValues}
        validationSchema={FormSchema}
      >
        <Form className={ContatoStyle["form-wrapper"]}>
          <strong className={ContatoStyle["form-title"]}>
            Preencha o Formulário
          </strong>
          <div className={ContatoStyle["fields-wrapper"]}>
            <label htmlFor="userName">Nome</label> <br />
            <Field name="name" placeholder="Seu nome completo" />
            <ErrorMessage
              component={"span"}
              name="name"
              className={ContatoStyle["form-invalid-feedback"]}
            />
          </div>
          <div className={ContatoStyle["fields-wrapper"]}>
            <label htmlFor="email">Email</label>
            <Field name="email" placeholder="Seu email" />
            <ErrorMessage
              component={"span"}
              name="email"
              className={ContatoStyle["form-invalid-feedback"]}
            />
          </div>
          <div className={ContatoStyle["fields-wrapper"]}>
            <label htmlFor="cpf">CPF</label>
            <Field name="cpf" placeholder="000 000 000 00" />
            <ErrorMessage
              component={"span"}
              name="cpf"
              className={ContatoStyle["form-invalid-feedback"]}
            />
          </div>
          <div className={ContatoStyle["fields-wrapper"]}>
            <label htmlFor="dateOfBirth">Data de Nascimento</label>
            <Field name="dateOfBirth" placeholder="00 . 00 . 0000" />
            <ErrorMessage
              component={"span"}
              name="dateOfBirth"
              className={ContatoStyle["form-invalid-feedback"]}
            />
          </div>
          <div className={ContatoStyle["fields-wrapper"]}>
            <label htmlFor="phone">Telefone</label>
            <Field name="phone" placeholder="(+00) 00000 0000" />
            <ErrorMessage
              component={"span"}
              name="phone"
              className={ContatoStyle["form-invalid-feedback"]}
            />
          </div>
          <div className={ContatoStyle["fields-wrapper"]}>
            <label htmlFor="instagram">Instagram</label>
            <Field name="instagram" placeholder="@seuuser" />
            <ErrorMessage
              component={"span"}
              name="instagram"
              className={ContatoStyle["form-invalid-feedback"]}
            />
          </div>
          <div className={ContatoStyle["declaracao-wrapper"]}>
            <strong>*Declaro que li e aceito</strong>
            <input type="checkbox" name="agreeCheckBox" id="agreeCheckBox" />
          </div>
          <input type="submit" value="Cadastre-se" />
        </Form>
      </Formik>
    </>
  );
}
