import React from "react";

import FooterStyles from "./Footer.module.css";
import Payments from "../../icons/payments/payment-flags.png";
import Banner from "../../icons/others/vtex-and-m3.png";

export const PageFooter = () => {
  return (
    <>
      <footer className={FooterStyles["footer"]}>
        <img
          src={Payments}
          alt="Métodos de Pagamento"
          className={FooterStyles["payments"]}
        />
        <p className={FooterStyles["lorem-text-mobile"]}>
          Lorem ipsum Dolor Sit Amet, Consectetur Adipiscing Elit..
        </p>

        <p className={FooterStyles["lorem-text-desktop"]}>
          Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing <br /> Elit, Sed Do
          Eiusmod Tempor
        </p>

        <a href="https://m3ecommerce.com/" target={"_blank"}>
          <img
            src={Banner}
            alt="Banner M3 e VTEX"
            className={FooterStyles["banner"]}
          />
        </a>
      </footer>
    </>
  );
};
