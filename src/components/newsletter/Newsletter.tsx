import React from "react";

import NewsStyles from "./Newsletter.module.css";

import WhatsApp from "../../icons/social/whatsapp.png";
import ArrowUp from "../../icons/others/arrow-up.png";

export const Newsletter = () => {
  return (
    <>
      <div className={NewsStyles["newsletter-container"]}>
        <div className={NewsStyles["newsletter-items-wrapper"]}>
          <strong>Assine Nossa Newsletter</strong>
          <span className={NewsStyles["input-button-wrapper"]}>
            <input
              type="email"
              name="newsEmail"
              id="newsEmail"
              placeholder="E-mail"
            />
            <button type="submit">Enviar</button>
          </span>
        </div>
        <div className={NewsStyles["arrow_up-and-whatsapp-wrapper"]}>
          <img src={WhatsApp} alt="" className={NewsStyles["whatsapp-icon"]} />
          <img src={ArrowUp} alt="" className={NewsStyles["arrow-up-icon"]} />
        </div>
      </div>
    </>
  );
};
