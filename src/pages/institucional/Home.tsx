import React from "react";

import { Routes, Route, Link } from "react-router-dom";

import Styles from "./Home.module.css";

import { Header } from "../../components/header/Header";
import { Main } from "../../components/main/MainContent";
import { Newsletter } from "../../components/newsletter/Newsletter";
import { Social } from "../../components/social/Social";
import { PageFooter } from "../../components/footer/Footer";

export const Home = () => {
  return (
    <>
      <div className={Styles["container"]}>
        <Header />
        <Main />
        <Newsletter />
        <Social />
        <PageFooter />
      </div>
    </>
  );
};
